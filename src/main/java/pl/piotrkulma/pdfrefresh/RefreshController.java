package pl.piotrkulma.pdfrefresh;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Piotr Kulma on 2017-09-22.
 */

@Controller
public class RefreshController {
    @Autowired
    private RefreshService refreshService;


    @RequestMapping(value = "/pdfrefresh", method=RequestMethod.GET)
    public String welcome() {
        return "pdfrefresh";
    }

    @ResponseBody
    @RequestMapping(value = "/checkForNewFile",  method=RequestMethod.GET)
    public boolean listener() throws IOException {
        return refreshService.isNewFileInPath();
    }

    @RequestMapping(value="newestFile", method=RequestMethod.GET)
    public void streamNewestFile(HttpServletResponse response) throws IOException {
        InputStream inputStream = refreshService.getNewestFileInputStream();

        if(inputStream == null) {
            return;
        }

        response.setContentType("application/pdf");
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
    }
}
