package pl.piotrkulma.pdfrefresh;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Optional;

/**
 * Created by Piotr Kulma on 2017-09-22.
 */

@Service
public class RefreshService {
    private static String lastFileName = "";

    @Value("${listening.path}")
    private String path;

    @Value("${listening.extension}")
    private String extension;

    public String geNewestFileName() throws IOException {
        Optional<Path> newFile = getNewestFileInPath();

        if(newFile.isPresent()) {
            lastFileName = newFile.get().getFileName().toString();
        }

        return lastFileName;
    }

    public boolean isNewFileInPath() throws IOException {
        Optional<Path> newFile = getNewestFileInPath();

        if(newFile.isPresent()) {
            return !newFile.get().getFileName().toString().equals(lastFileName);
        }

        return false;
    }

    public InputStream getNewestFileInputStream() throws IOException {
        lastFileName = geNewestFileName();
        if(StringUtils.isEmpty(lastFileName)) {
            return null;
        }

        return new FileInputStream(path + "/" + lastFileName);
    }

    private Optional<Path> getNewestFileInPath() throws IOException {
        Optional<Path> newestFile =
                Files.list(Paths.get(path))
                        .filter(p -> p.getFileName().toString().endsWith(extension))
                        .sorted(
                                (p1, p2) -> compareTwoFilesByLastModifiedTime(p1, p2))
                        .findFirst();

        return newestFile;
    }

    private int compareTwoFilesByLastModifiedTime(Path p1, Path p2) {
        try {
            return Files.readAttributes(p2, BasicFileAttributes.class).lastModifiedTime().compareTo(Files.readAttributes(p1, BasicFileAttributes.class).lastModifiedTime());
        } catch (Exception e) {
            return 0;
        }
    }
}
