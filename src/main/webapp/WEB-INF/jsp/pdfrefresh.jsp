<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
    <head>
        <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <script type="text/javascript">
            function checkForUpdate() {
                $.get("/checkForNewFile", function (data) {
                    if(data == true) {
                        window.location.reload();
                    }
                });
            }

            $(function() {
                setInterval(checkForUpdate, 1000);
            });
        </script>

        <div id="fileContent">
            <embed src="/newestFile" width="800" height="900" type='application/pdf'>
        </div>
    </body>
</html>